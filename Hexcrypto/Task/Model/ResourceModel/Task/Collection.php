<?php
namespace Hexcrypto\Task\Model\ResourceModel\Task;

use Hexcrypto\Task\Model\ResourceModel\Task as ResourceTask;
use Hexcrypto\Task\Model\Task;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
 
    protected function _construct()
    {
        $this->_init(Task::class, ResourceTask::class);
    }
}
